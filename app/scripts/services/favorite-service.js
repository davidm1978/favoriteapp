'use strict';


favoriteApp.service('favoriteService', ['$http', '$q', function ($http, $q) {


  this.favorites = [];


  this.getProvidersFromApi = function () {
    var deferred = $q.defer();
    $http({
      method: 'GET',
      url: 'https://restcountries-v1.p.mashape.com/all',
      headers: {
        'X-Mashape-Key': 'BylaeYEtvQmshLhAiqryXiqZN26Vp1Fr6g3jsn5Kutx52GAD0H'
      },
     cache: true
    }).success(function (result) {
      deferred.resolve(result);
    })
      .error(function (err) {
        deferred.reject(err);
      });
    return deferred.promise;
  };

}]);
