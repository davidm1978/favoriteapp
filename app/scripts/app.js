'use strict';

/**
 * @ngdoc overview
 * @name favoriteApp
 * @description
 * # favoriteApp
 *
 * Main module of the application.
 */
var favoriteApp = angular.module('favoriteApp', ['ui.router','smart-table','checklist-model']);

favoriteApp.config(['$stateProvider','$urlRouterProvider',  function($stateProvider,$urlRouterProvider){

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('root',{
      url: '',
      abstract: true,
      views: {
        'header': {
          templateUrl: 'scripts/view-model/header/header-view.html'
        },
        'footer': {
          templateUrl: 'scripts/view-model/footer/footer-view.html'
        }
      }
    })
    .state('root.provider', {
      url: '/',
      views: {
        'container@': {
          templateUrl: 'scripts/view-model/provider/provider-view.html',
          controller: 'providerCtrl'
        }
      }
    })
    .state('root.favorite', {
      url: '/favorite',
      views: {
        'container@': {
          templateUrl: 'scripts/view-model/favorite/favorite-view.html',
          controller: 'favoriteCtrl'
        }
      }
    });
}]);
