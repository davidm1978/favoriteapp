'use strict';


favoriteApp.controller('providerCtrl',['$scope','favoriteService', function ($scope,favoriteService) {
  $scope.providerList = [];
  $scope.itemsByPage = 10;
  $scope.favoriteService = favoriteService;



  var getProviderList = function(){
    favoriteService.getProvidersFromApi().then(function (result) {
      $scope.providerList = result;
    }, function (err) {
      //handle error
      console.log(err);
    });
  };



  function init() {
    getProviderList();
  }


  init();


}]);
